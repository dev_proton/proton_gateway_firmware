# Proton_Gateway_Firmware
```
VERSIONINFO = {
  DEVICE_TYPE = "PROTON-GTW",
  FIRMWARE_VER = "1.4-rc3_CELL_iot_GW1",
  HARDWARE_CODE = "PROTON-GTW",
  NOTE = "=). Proton"
}

RELEASE_NOTES = {
  v1_4_rc3 = "29/09/23 Compatibilty with Personal Hotspots",
  v1_4_rc2 = "28/09/23 Compatibilty with V1.2 config files",
  v1_4_rc1 = "20/09/23 Fix bug with libluaeio libs",
  v1_4_rc0 = "15/09/23 Includes Opensslv3 libs for AzureIoT and SMTP compatibility",
  v1_3_rc1 = "20/01/20 Includes Dual Ethernet basic support",
  v1_3_rc0 = "19/12/19 Includes SNMP beta version 1",
  v1_2_rc8 = "19/01/20 Updated MAC re-creation",
  v1_2_rc7 = "25/11/19 Updated WiFi connection, improved sqlite queries by adding index",
  v1_2_rc6 = "13/11/19 Added Import/Export function on Charts",
  v1_2_rc5 = "25/10/19 Added IMEI number on Cellular Status",
  v1_2_rc4 = "19/10/19 Fixed Modbus 485 (GPS and Celullar versions)",
  v1_2_rc3 = "28/09/19 Configure GPS and NTP",
  v1_2_rc2 = "28/09/19 Configure Celullar modem"
}

```
# X.Y.Z Version Coding Scheme

In this coding version scheme three digits are used for version coding. Most significant digit will change
in order to reflect important modifications. These can involve some backward incompatibilities. Versions 
that are coded with the pattern 0.Y.Z mean that implementation lives up to minimum requirements but it is
not fully functional. Each change to the most significant digit can involve tasks like code refactoring, 
module redesign or a bunch of new feature additions.

The second digit X.0.Z should be changed when minor functionalities are added or fixed. This involve minor
bug fixing or minor functionalities additions that keep backward compatibilities.

Finally third digit X.Y.0 is changed when performed changes won't be noted by user. These are modifications
that don't change user interaction with the application.

It is expected that a change in a particular digit will conduct right side digit initialization to zero. 
When not fully teste versions are released, it is common to add a letter to the right end. For example
if the current system version is 1.9.0, and before releasing a version 2.0 with a bunch of new features
, this version must be tested on field, this software could be released as version 1.9.0a or 2.0a. If 
during field testing some other changes are added, these versions can be named 1.9.0b or 2.0b. Another 
approach makes use of the acronym RC which stands for Release Candidate. Using previous example, the version
coding would something like 2.0-RC1 and 2.0-RC2

## More Information Links
[Semantic Versioning 2.0.0](https://semver.org/)

[Semantic Versioning 2.0.0-rc.1](https://semver.org/spec/v2.0.0-rc.1.html)


## Proton Version Glosary
* Celullar9: Cellular modem 9600bps
* Celullar115: Cellular modem 11500bps
* GPS: GPS compability
* iot: Cloud Access
* GW1: Gateway Celular version


